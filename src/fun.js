export default {
  install(Vue) {
    //数组加删除方法
    Array.prototype.remove = function(val) {
      let index = this.indexOf(val);
      if (index > -1) {
        this.splice(index, 1);
      } else {
        console.log(val + "不存在");
        return false;
      }
    };
    //刷新当前页面
    Vue.prototype.$reflashPage = function() {
      let NewPage = "_empty" + "?time=" + new Date().getTime() / 500;
      this.$router.push(NewPage);
      this.$router.go(-1);
    };
    //判断是否为空
    Vue.prototype.$isEmpty = v => {
      switch (typeof v) {
        case "undefined":
          return true;
        case "string":
          if (v.replace(/(^[ \t\n\r]*)|([ \t\n\r]*$)/g, "").length == 0)
            return true;
          break;
        case "boolean":
          if (!v) return true;
          break;
        case "number":
          if (0 === v || isNaN(v)) return true;
          break;
        case "object":
          if (null === v || v.length === 0) return true;
          for (let i in v) {
            return false;
          }
          return true;
      }
      return false;
    };
  }
};
