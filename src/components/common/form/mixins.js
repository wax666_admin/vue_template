//获取表格数据
export const GetTableData = {
  props: ["formDatas", "params"],
  data() {
    return {
      fields: [],
      tableDatas: {},
      totalCount: 0,
      curDatas: {}
    };
  },
  created: function() {
    this.init(this.formDatas);
  },
  methods: {
    init(curVal) {
      if (curVal.showType === "table" || curVal.showType === "row") {
        this.getTableData(curVal.tableDataUrl);
        this.fields = curVal.fields;
      }
    },
    //table、row-获取表单数据
    getTableData(url) {
      this.$http
        .get(url, {
          params: this.params
        })
        .then(res => {
          let data = res.data;
          this.tableDatas = data.rows;
          this.totalCount = data.totalCount;
          this.curDatas = JSON.parse(JSON.stringify(this.tableDatas));
        });
      /*      this.reload = false;*/
    },
    //代码处理
    handelCode(data, item) {
      let x = (item.fieldCode + "***").split("dm***");
      let labelCode = x[1] === "" && data.hasOwnProperty(x[0]) ? x[0] : null;
      let newItem = JSON.parse(JSON.stringify(item));
      Object.assign(newItem, {
        defaultValue: data[item.fieldCode],
        defaultLabel: data[labelCode]
      });
      return newItem;
    }
  }
};

//table、row编辑状态-公共代码
export const CurEdit = {
  props: ["addBtnShow"],
  data() {
    return {
      addList: {},
      addFlag: false,
      editFlag: false
    };
  },
  computed: {
    curTitle() {
      return this.formDatas.fields.filter(function(item) {
        return !item.hidden;
      });
    }
  },
  watch: {
    curDatas: {
      handler(curVal) {
        this.$emit("update:curDatas", curVal);
      },
      deep: true
    }
  },
  methods: {
    //编辑
    editList(item) {
      if (typeof item.editFlag === "undefined") {
        this.$set(item, "editFlag", true);
      } else {
        item.editFlag = !item.editFlag;
      }
    },
    //删除
    deleteList(item) {
      if (typeof item.active === "undefined") {
        this.$set(item, "active", true);
      } else {
        item.active = true;
      }
      this.$confirm("确定删除这条数据?", "提示", {
        type: "warning"
      })
        .then(() => {
          let params = this.$qs.stringify(item);
          this.$http.post(this.formDatas.tableDeleteUrl, params).then(res => {
            if (res.code === 0) {
              this.$message.success("删除成功！");
              this.curDatas.remove(item);
            }
          });
        })
        .catch(() => {
          item.active = false;
        });
    },
    deleteAdd() {
      this.addFlag = false;
      this.addList = {};
    },
    //保存
    saveList(formName, item) {
      let refsForm =
        formName === "addList" ? this.$refs[formName] : this.$refs[formName][0];
      refsForm.validate(valid => {
        if (valid) {
          let params = JSON.parse(JSON.stringify(item));
          this.$delete(params, "editFlag");
          Object.assign(params, this.params);

          //params = this.$qs.stringify(params);
          this.$http.post(this.formDatas.saveUrl, params).then(res => {
            if (res.code === 0) {
              this.$message.success("保存成功！");
              if (formName === "addList") {
                refsForm.resetFields(); //重置表单
                this.addFlag = false;
              } else {
                item.editFlag = false;
              }
              this.getTableData(this.formDatas.tableDataUrl); //刷新表单
            }
          });
        } else {
          return false;
        }
      });
    },
    //取消
    cancelList(liItem, index) {
      liItem.editFlag = false;
      for (let key in this.tableDatas[index]) {
        liItem[key] = this.tableDatas[index][key];
      }
      this.$refs["liItem" + index][0].validate();
    }
  }
};
