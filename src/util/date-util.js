
/*
//使用
getToday() //2018-02-12
getToday('zw') //2018年02月12日
getLunarCalendar() //戊戌年 十月廿七
getWeek().zw getWeek().sz //二  2
getCurMonth().firstDay ~ getCurMonth().lastDay//本月：2018-12-01 ~ 2018-12-31
getCurWeek().firstDay ~ getCurWeek().lastDay //本周：2018-12-03 ~ 2018-12-09
getEveryDay(getCurWeek().firstDay) //本周一至今的每一天：2018-12-03，2018-12-04，2018-12-05
getTargetDay('-7') //7天前：2018-11-27

import {getToday,getWeek,getCurMonth,getCurWeek,getEveryDay,getTargetDay,getLunarCalendar} from '@/util/date-util'
*/


const oneDay = 86400000; //一天1000*60*60*24毫秒

//格式化日期
function formatDate(date, zw = false) {
  let y = date.getFullYear(),
    m = date.getMonth() + 1,
    d = date.getDate();
  m = m < 10 ? "0" + m : m;
  d = d < 10 ? "0" + d : d;
  if (zw) {
    return y + "年" + m + "月" + d + "日";
  } else {
    return y + "-" + m + "-" + d;
  }
}

//获取当天时间
export const getToday = function(flag) {
  if (flag === "zw") {
    return formatDate(new Date(), true);
  } else {
    return formatDate(new Date());
  }
};

//获取当前星期
export const getWeek = function(day) {
  let date = day === undefined ? new Date() : new Date(day);
  let sz = date.getDay();
  let zw = "日一二三四五六".charAt(sz);
  return { sz, zw };
};

//获取当前月的第一天和最后一天
export const getCurMonth = function(day) {
  let date = day === undefined ? new Date() : new Date(day);
  let currentMonth = date.getMonth();
  let nextMonth = ++currentMonth;
  let nextMonthFirstDay = new Date(date.getFullYear(), nextMonth, 1);
  let lastDay = formatDate(new Date(nextMonthFirstDay - oneDay));
  date.setDate(1);
  let firstDay = formatDate(date);
  return { lastDay, firstDay };
};

//获取本周的第一天和最后一天
export const getCurWeek = function(day) {
  let date = day === undefined ? new Date() : new Date(day);
  let first = date - (date.getDay() - 1) * oneDay;
  let last = first + oneDay * 6;
  let lastDay = formatDate(new Date(last));
  let firstDay = formatDate(new Date(first));
  return { lastDay, firstDay };
};

//获取时间区间内每一天
export const getEveryDay = function(startTime, endTime = new Date()) {
  let days = [];
  for (let i = Date.parse(startTime); i <= Date.parse(endTime); i += oneDay) {
    let day = formatDate(new Date(i));
    days.push(day);
  }
  return days;
};

//获取最近第几天
export const getTargetDay = function(day, n) {
  //n表示最近几天，负数为day前几天
  let date;
  if (n === undefined) {
    n = day;
    date = new Date();
  } else {
    date = new Date(day);
  }
  let targetDay = formatDate(new Date(date.getTime() + oneDay * n));
  return targetDay;
};

//农历 showCal
//定义全局变量
var CalendarData = new Array(100);
var madd = new Array(12);
var tgString = "甲乙丙丁戊己庚辛壬癸";
var dzString = "子丑寅卯辰巳午未申酉戌亥";
var numString = "一二三四五六七八九十";
var monString = "正二三四五六七八九十冬腊";
//var weekString = "日一二三四五六";
//var sx = "鼠牛虎兔龙蛇马羊猴鸡狗猪";
var cYear, cMonth, cDay, TheDate;
CalendarData = new Array(
  0xa4b,
  0x5164b,
  0x6a5,
  0x6d4,
  0x415b5,
  0x2b6,
  0x957,
  0x2092f,
  0x497,
  0x60c96,
  0xd4a,
  0xea5,
  0x50da9,
  0x5ad,
  0x2b6,
  0x3126e,
  0x92e,
  0x7192d,
  0xc95,
  0xd4a,
  0x61b4a,
  0xb55,
  0x56a,
  0x4155b,
  0x25d,
  0x92d,
  0x2192b,
  0xa95,
  0x71695,
  0x6ca,
  0xb55,
  0x50ab5,
  0x4da,
  0xa5b,
  0x30a57,
  0x52b,
  0x8152a,
  0xe95,
  0x6aa,
  0x615aa,
  0xab5,
  0x4b6,
  0x414ae,
  0xa57,
  0x526,
  0x31d26,
  0xd95,
  0x70b55,
  0x56a,
  0x96d,
  0x5095d,
  0x4ad,
  0xa4d,
  0x41a4d,
  0xd25,
  0x81aa5,
  0xb54,
  0xb6a,
  0x612da,
  0x95b,
  0x49b,
  0x41497,
  0xa4b,
  0xa164b,
  0x6a5,
  0x6d4,
  0x615b4,
  0xab6,
  0x957,
  0x5092f,
  0x497,
  0x64b,
  0x30d4a,
  0xea5,
  0x80d65,
  0x5ac,
  0xab6,
  0x5126d,
  0x92e,
  0xc96,
  0x41a95,
  0xd4a,
  0xda5,
  0x20b55,
  0x56a,
  0x7155b,
  0x25d,
  0x92d,
  0x5192b,
  0xa95,
  0xb4a,
  0x416aa,
  0xad5,
  0x90ab5,
  0x4ba,
  0xa5b,
  0x60a57,
  0x52b,
  0xa93,
  0x40e95
);
madd[0] = 0;
madd[1] = 31;
madd[2] = 59;
madd[3] = 90;
madd[4] = 120;
madd[5] = 151;
madd[6] = 181;
madd[7] = 212;
madd[8] = 243;
madd[9] = 273;
madd[10] = 304;
madd[11] = 334;

export const getLunarCalendar = function() {
  var D = new Date();
  var yy = D.getFullYear();
  var mm = D.getMonth() + 1;
  var dd = D.getDate();
  //var ww = D.getDay();
  //var ss = parseInt(D.getTime() / 1000);
  if (yy < 100) yy = "19" + yy;
  return GetLunarDay(yy, mm, dd);
};
function GetBit(m, n) {
  return (m >> n) & 1;
}
//农历转换
function e2c() {
  TheDate =
    arguments.length != 3
      ? new Date()
      : new Date(arguments[0], arguments[1], arguments[2]);
  var total, m, n, k;
  var isEnd = false;
  var tmp = TheDate.getYear();
  if (tmp < 1900) {
    tmp += 1900;
  }
  total =
    (tmp - 1921) * 365 +
    Math.floor((tmp - 1921) / 4) +
    madd[TheDate.getMonth()] +
    TheDate.getDate() -
    38;

  if (TheDate.getYear() % 4 == 0 && TheDate.getMonth() > 1) {
    total++;
  }
  for (m = 0; ; m++) {
    k = CalendarData[m] < 0xfff ? 11 : 12;
    for (n = k; n >= 0; n--) {
      if (total <= 29 + GetBit(CalendarData[m], n)) {
        isEnd = true;
        break;
      }
      total = total - 29 - GetBit(CalendarData[m], n);
    }
    if (isEnd) break;
  }
  cYear = 1921 + m;
  cMonth = k - n + 1;
  cDay = total;
  if (k == 12) {
    if (cMonth == Math.floor(CalendarData[m] / 0x10000) + 1) {
      cMonth = 1 - cMonth;
    }
    if (cMonth > Math.floor(CalendarData[m] / 0x10000) + 1) {
      cMonth--;
    }
  }
}
function GetcDateString() {
  var tmp = "";
  //显示农历年：（ 如：甲午(马)年 ）
  tmp += tgString.charAt((cYear - 4) % 10);
  tmp += dzString.charAt((cYear - 4) % 12);
  /*tmp+="(";
    tmp+=sx.charAt((cYear-4)%12);*/
  tmp += "年  ";
  if (cMonth < 1) {
    tmp += "(闰)";
    tmp += monString.charAt(-cMonth - 1);
  } else {
    tmp += monString.charAt(cMonth - 1);
  }
  tmp += "月";
  tmp += cDay < 11 ? "初" : cDay < 20 ? "十" : cDay < 30 ? "廿" : "三十";
  if (cDay % 10 != 0 || cDay == 10) {
    tmp += numString.charAt((cDay - 1) % 10);
  }
  return tmp;
}

function GetLunarDay(solarYear, solarMonth, solarDay) {
  //solarYear = solarYear<1900?(1900+solarYear):solarYear;
  if (solarYear < 1921 || solarYear > 2020) {
    return "";
  } else {
    solarMonth = parseInt(solarMonth) > 0 ? solarMonth - 1 : 11;
    e2c(solarYear, solarMonth, solarDay);
    return GetcDateString();
  }
}

