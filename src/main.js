// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App";
import router from "./router";
import axios from "./http";
import fun from "./fun";
Vue.use(fun);
import qs from "qs";
Vue.prototype.$qs = qs;

import Vuex from "vuex";
Vue.use(Vuex);
import store from "./store/store";
/*import $ from "jquery";*/
const $ = require("jquery");
window.$ = $;

import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
Vue.use(ElementUI);

import common from "@/components/common";
Vue.use(common);

import "@/assets/css/style.scss";

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: "#app",
  $,
  router,
  axios,
  store,
  components: { App },
  template: "<App/>"
});
