const path = require("path");

function resolve(dir) {
  return path.join(__dirname, dir);
}
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");

module.exports = {
  //baseUrl: './',// baseUrl  type:{string} default:'/'
  //baseUrl: process.env.NODE_ENV === 'production' ? '/online/' : '/',
  outputDir: "dist", // 输出文件目录
  assetsDir: "static", // 资源目录
  lintOnSave: undefined, // 是否使用eslint
  productionSourceMap: false, // 如果您不需要生产时的源映射，那么将此设置为false可以加速生产构建

  // css相关配置
  css: {
    //extract: true, // 是否使用css分离插件 ExtractTextPlugin
    sourceMap: true, // 开启 CSS source maps?
    //modules: false, // 启用 CSS modules for all css / pre-processor files.
    loaderOptions: {
      // css预设器配置项
      sass: {
        //data: `@import "@/variables.scss";`// 向所有 Sass 样式传入共享的全局变量
      }
    }
  },

  /*
        构建多页面模式的应用程序.每个“页面”都应该有一个相应的JavaScript条目文件。该值应该是一
        个对象，其中键是条目的名称，而该值要么是指定其条目、模板和文件名的对象，要么是指定其条目
        的字符串，
        注意：请保证pages里配置的路径和文件名 在你的文档目录都存在 否则启动服务会报错的
      */
  // pages: {
  // index: {
  // entry for the page
  // entry: 'src/index/main.js',
  // the source template
  // template: 'public/index.html',
  // output as dist/index.html
  // filename: 'index.html'
  // },
  // when using the entry-only string format,
  // template is inferred to be `public/subpage.html`
  // and falls back to `public/index.html` if not found.
  // Output filename is inferred to be `subpage.html`.
  // subpage: 'src/subpage/main.js'
  // },

  // webpack配置  see https://github.com/vuejs/vue-cli/blob/dev/docs/webpack.md
  //允许对内部的 webpack 配置进行更细粒度的修改。
  chainWebpack: config => {
    config.entry("index").add("babel-polyfill");
  },

  configureWebpack: config => {
    config.resolve = {
      extensions: [".js", ".vue", ".json"],
      alias: {
        vue$: "vue/dist/vue.esm.js",
        "@": resolve("src")
      }
    };

    //入口文件
    //config.entry.app = ['./src/main.js'];
    if (process.env.NODE_ENV === "production") {
      // 为生产环境修改配置...
      config.mode = "production";
    } else {
      // 为开发环境修改配置...
      config.mode = "development";
    }
    //删除console插件
    let plugins = [
      new UglifyJsPlugin({
        uglifyOptions: {
          compress: {
            warnings: false,
            drop_console: true,
            drop_debugger: true
          },
          output: {
            // 去掉注释内容
            comments: false
          }
        },
        sourceMap: false,
        parallel: true
      })
    ];
    //只有打包生产环境才需要将console删除
    if (process.env.VUE_APP_build_type == "production") {
      config.plugins = [...config.plugins, ...plugins];
    }
  },

  // webpack-dev-server 相关配置
  devServer: {
    port: 8085, // 端口号
    //host: 'localhost',//不设置显示localhost和ip两个
    https: false, // https:{type:Boolean}
    //open: true, //配置自动启动浏览器
    // open: process.platform === 'darwin',
    hotOnly: false, //
    proxy: {
      "/api": {
        target: "http://10.71.19.216:10001",
        ws: true, //
        changeOrigin: true
      },
      "/test": {
        target: "https://easy-mock.com/mock/5c8090e5479d5e248a6f0c07",
        ws: true, //
        changeOrigin: true
      }
    }
  },
  // 第三方插件配置
  pluginOptions: {}
};
